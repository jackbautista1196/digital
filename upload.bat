@echo off
setlocal

REM Ruta de la carpeta con los archivos que deseas cargar
set "carpeta_archivos=C:\filesUpload"

REM URL del repositorio en GitLab
set "url_repo=https://gitlab.com/jackbautista1196/digital.git"

REM Nombre de la rama en la que deseas cargar los archivos
set "nombre_rama=main"

REM Inicializar el repositorio local
git init

REM Agregar y confirmar los archivos en la carpeta
git add .
git commit -m "Carga inicial de archivos 2"

REM Agregar la URL del repositorio remoto


REM Subir los archivos a la rama especificada
git push origin "%nombre_rama%"

REM Finalizar y mostrar mensaje
echo Archivos cargados exitosamente en GitLab.

pause
